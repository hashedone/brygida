# Brygida

Brygida is AI powered Contract Bridge software, meant to be tool for learning bridge.

## Components

As bridge is a multi-layer game, Brigida would be divided into several parts responsible for different aspects of the game.

### Double dummy solver

The core part is double dummy game solver, which finds best play for any given bridge state, given that all hands are known. It is probably the simplest part of engine, but also it is critical to be very fast, as at the and many hands would be tried to have reaasonable prediction on actuall best play (or bid).

For now I assume some kind of simple implementation based on [MCTS](https://pl.wikipedia.org/wiki/Monte-Carlo_Tree_Search), but I believe there are better solutions known, as this is well known problem already - migrating to something more efficient is possible. Also I consider some influence of neural network as a heuristic.

### Hand constraint solver

The game of bridge heavely depends on some knowledge about hands, which limits what is expected in hidden hands. The easiest constraint is, that if some point player lays a card not of lead suit, that player can no longer have cards of this suit. However to play competetive bridge with reasonable decisions about finesing or proper defensive leading, or even earlier - bidding, it is needed to figure out additional information. Some intel might be gathered from the auction itself. However it is not easy to determine what constraints should be set. The obvious one is expected length in every of four colors. However very important thing is also to determine where are high cards located, as this is what actually determines number of tricks made by the side. There are many ways to constraint it. The easy is that, there are some bidds which precisely locates high cards (cue-bids, artificial key cards/kings/queens questions), so those kind of constraints has to be possible. The hard think is to properly constaint honors which are not preciesely placed. The most common way is to calculate what is called HCP (high card poits) - 4HCP for A, 3HPC for K, 2HPC for Q and 1HPC for J. This is common approach for modern bridge, so constraining on HCP have to be implemented even to properly implement common bidding systems.

However I am not sure if it is the best way to evaluate (and constraint) honors. It is because obiously AK in long color (almost) always gives 2 tricks, AKD 6-card longer can often secure 6 tricks. On the other hand single K is basically 0 tricks in many situations, and even singleton ace sometimes doesn't guarantee additional trick (when it was previously guaranteed as a first-class holding). Because of that I would like to also implement the OSIKA expected trick system developed long time ago by polish bridge player Łukasz Sławiński. Instead of assigning arbitral point value for each honor, he assigns expected trick-quaters (I would call it QP - quater points) for every honor, as follows:

| Card | QP | HPC |
|------|----|-----|
| A    | 4  | 4   |
| K    | 3  | 3   |
| D    | 2  | 2   |
| J    | 0  | 1   |

This means, that Ace is expected to always provide a trick, kind should provide a trick in 3 of 4 cases, and so on. The only difference from HPC for now is that J has assigned 0 points (as it actually almost never provide trick on its own).

The difference from HPC is, that this is not the end of points calculations - Sławiński notes, that the honors themselve are not equally strong in different hands, it is actually how are they distributes (as KQ is ofter 2 tricks on ace finese, and ADJ is basically 3 tricks on king finese). This is why we add additional points for honors focus:

* two honors in color adds 2QP
* three honors in colors adds 3QP
* 09 (both, not one) in color wiht at least one honor adds 1QP (as it often help properly playing out honor)

So KQ in color is now 3 + 2 + 2 = 7QP (1.75 expected tricks), and ADJ is 4 + 2 + 0 + 3 = 9 (2.25 expected tricks).

### Best move solver

Kind of additionall layer above dummy solver. For now the idea is to pick the best move, having only public information: my hand, the dummy (except first lead), hands constraints (which ancodes auction and what was already played), and the contract. The implementation I have in mind is basically Monte Carlo:

* generate number of random hands fullfilling contracts
* calculate number of tricks taken after each possible move for each generated games
* for each possible move calculate points it would generate in all games, basing on tricks taken, and the contract
* for each possible move calculate expected points it generate basing on points generated in games, multiplied of probablility of this exact points generated; if lying queen of hearts gives 60 points (+2 tricks on 1H contract) on 25% king finese, but may cost -50 on missing it (-1 trick), the expected points of QH is `60 * 0.25 + (-50) * 0.75 = -22.5`
* pick move with the best expected points

The algorithm can be improved in at least two ways:

* pruning number of generated hands, by either smart generating relevant hands and estimating its importance (statistical number of hands with exactly the same outcome), or unification of similar hands (so they are generated, but tricks value are calculated only for one of similar hands, considering that for similar hands it would be the same); NN assistance is considered in this
* prinning considered moves (as leading A or K of same suit is basically the same, also leading 3 in AKJ873 is nonsense - none of those need to be considered)

### Bidding hands predictor

The utility which considering the auction is able to build hands constraints for future plays. It is required both for playing phase (so moves values can be estimated properly), but also for an auction (so bidds are performed with better information about all 4 hands).

### Bidder

Auction is actually a separate game. It is separated topic, and its goal is to set up a contract, which would give the best outcome for possible hands layout. There are many aspects of bidding:

* Information is way more limited in this stage, as there is no buddy, and not even full auction inforation
* It is unclear what is a goal of single bid - sometimes it is to find the best contract on already have information, but it might be to improve knowledge of the table (when I already know, that 4S is playable, I might want to try find out if 7S is also a thing, so maybe 4S is not the best bid yet)
* Not all bids are meant to be played (as there are forcing bids, and even artificial one), however it is actually not clear if this is true - Sławiński created whole TELIMENA bridge theory in which he claimed, that it is possible to have perfect fully naturall auction system (both well defined and giving best result), having rule "What is bidden it is proper to be played" in mind.

Also there is not clear what actually "natural" or "forcing" means, and rules of bridge auctions are complicated. Things which are very obvious for human, might be not so clear for computers (when to alert, if a bid is a splinter or color, if double is takeout or penalty).

Considering this I plan two bidding engines.

#### System-constrained bidding engine

The traditional bidding engine, meant to be used for teaching and playing with humans. The idea is to create language for describing auction systems, which would describe meaning of bids in context. This is not so simple task, as it is impossible to describe all bids one by one, so the language should allow for some generic explanation. The idea is not to strictly define which bid would be the best for given hand, but which bids are allowed by system in given hand (so eg. in SAYC, 2C openning is illegal for 15HPC hand).

The language is processed by bidding engine, and when its time to bid, all valid bids considering auction till current point and the hand are choosen.

When its done, prediction phase starts. As for playout, engine generates random possible hands for all players, and evaluates possible game outcomes (tricks taken - not expected points, but tricks taken). Then it decides what is the best contract for this status, and what is expected point for such contract. This gives knowledge about which contracts are probable to be played and with what chance. Then difficult part comes into play - simulating bids, and figuring out how things might happen after bidding any reasonable legal bid at this point. The bid with best chance of ending out with highes after-game evaluation, is choosen and bid.

Additional upgrade is, that when hands are randomised, the engine starts simulation from the beginnign for each hand to decide if it is possible, that with such a card, this bid would be choosen - however it is possiblle, that another neural network would be better suited for this job.

The upside of this approach is that it is predictable, and seems to be best fit for auction rules (having convention cards, and similar things). The bad thing is, that it looks very complicated and restricted. That is why I think about another idea.

#### Non-constrained NN bidding engine

The other approach is to develop deep learning based bidding engine, which doesn't care about natuality of auction or any kind of rules - it just want to find out the best bidding system, and not to care if it is understandable for humans. The idea is simple - neural network gets its hand, and auction till current point as input, and it should output a bid it would take. NN outputs are obviously all bids (including pass, double, redouble), and they are probablility values. Legal bid with highes probability is played (legality is only based on technical game rules - so double can be only responded for non-double bid of opponents, redouble is only response for opponents double, and you have to bid higher than anyone bid prevoisly).

The important here is the training method. First of all - for training phase, choosing the bid method is changed slightly - instead of picking bid of the best probability, the bid is picked by random, but with weights being the probabilities of bid. Next, the tournament would be set up - some arbitral number of pairs would be playing the same bid, and then the deal would be played out by double-dummy bridge solver (as it is the best, if auction would help figuring out relevant hidden information). After all every pair score is evaluated (basing on points considering final contract), and some scoring system is applied to evaluate particular auction performance (either max-points or IMPs or another evaluation). The final output is used to reward or penalise particular pair  auction.

Possibly this can be also improved by predicting possible playouts just like in system-constrained bidding engine, but it is next step.

This kind of engine can be used to figure out what AI things about bidding and bidding system, and maybe better undestand what is important for bidding. Maybe it could be even used to help with designing best auction system for contract bridge.

### Constaints predictor

The last part of the puzzle, needed for non-constained NN bidding engine. This tool uses whole already known information (auction, own hand, dummy, played tricks) to determine the constaints for all hidden hands. This would be used in two ways:

* gives constraints input to the best move solver to find the best move
* gives information about bidding meaning for other players (so auction is not exactly a mystery for opponents)
